# Programar que presente una calificacion cualitativa
# de acuerdo a ciertas equivalentes
# Crear una funcion llamada calculo_calificacion
#   autor   =   "Michael Vasquez"
#   email   =   "michael.vasquez@unl.edu.ec"

# definida la funcion:
def calcula_calificacion(a):
    if a >= 0  and a  <=  1.0:
        if a >=  0.9:
            print("Sobresaliente")
        elif a >=  0.8:
            print("Notable")
        elif a >=  0.7:
            print("Bien")
        elif a <=  0.6:
            print("Insuficiente")
    else:
        print("Fuera del rango")

# Empeiza el proceso
try:
    puntuacion  =   float(input("Introduzca puntuacion:\n"))
    calcula_calificacion(puntuacion)
except:
    print("Puntuacion incorrecta")
