#   si la tarifa sobrepasa de 40 se le dara al
#   cliente 1.5 veces mas
#   Crear una funcion llamada calculo_salario
#   autor   =   "Michael Vasquez"
#   email   =   "michael.vasquez@unl.edu.ec"

# definimos la funcion con los parametros
def calculo_salario (s,t):
    if s <= 40:
        total  =   s   *   t
        print("La cantidad que ustede resive es de ", total, " dolares")
    elif s >= 41:
        s   -=  40
        total   =   40   *   t
        incre   =   1.5 * (s * t)
        total   +=  incre
        print("La cantidad que ustede resive es de ", total, " dolares")
    else:
        print("La cantidad que Usted ingreso es incorrecta")

# Empieza el proceso
try:
    saldo   =   float(input("Ingrese el numero de horas trabajadas:\n"))
    tarifa  =   float(input("Engrese el monto por hora:\n"))
    calculo_salario(saldo,tarifa)

except:
    print("La cantidad que Usted ingreso es incorrecta")
